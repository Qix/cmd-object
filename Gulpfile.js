var gulp = require('gulp');
var coffee = require('gulp-coffee');

gulp.task('default', function() {
  gulp.src('./src/**/*.coffee', {base: './src'})
      .pipe(coffee())
      .pipe(gulp.dest('lib'));
});
