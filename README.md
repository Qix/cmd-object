~~~
     _____           _       _____  _      _            _
    |     | _____  _| | ___ |     || |_   |_| ___  ___ | |_
    |   --||     || . ||___||  |  || . |  | || -_||  _||  _|
    |_____||_|_|_||___|     |_____||___| _| ||___||___||_|
                                        |___|
~~~

Cmd-Object is a little library that is used to turn objects
into command line parameters.

## Usage
All examples implicitly include cmd-object using:
```
var CmdObject = require('cmd-object');
```

Cmd-Object is used by creating a new `CmdObject` object with a base command.
This base command is simply a simple executable name that begins the generated
command line.

```
var javaCmd = CmdObject('java');
```

From there, command line arguments can be specified.
This is done by specifying a property selector (`$()`), which reads a property
of the object passed to `generate()` and uses its value for all subsequent
transformations (until another selection occurs or `generate()` is called).
```
javaCmd.$('classpath').join(path.delimiter).wrap().prefix('-cp ')
       .$('jar').wrap().prefix('-jar ');
```

The above example will first take `obj.classpath` from the object passed to
`generate(obj)` and

 1. Join the array using `path.delimiter` character
 2. Wrap that string (using the default, double quotes)
 3. Prefix the quoted classpath string with the `-cp` flag and a space

Then it will proceed to take the `obj.jar` value and

 1. Wrap the value using double quotes
 2. Prefix it with `-jar` and a space

From there, you can now call `generate()` given the parameters, such as
```
var javaArgs = {
  classpath: [
    '.',
    './bin',
    './lib/*'
  ],
  jar: './someJar.jar'
};

javaCmd.generate(javaArgs, function(cmdLine){
  console.log(cmdLine);
});
```

The above will output:

> java -cp ".:./bin:./lib/*" -jar "./someJar.jar"

## Extending

If you'd like to extend the API (and the transformations available for
selections), simply add funtions to `CmdObject.API` that accept a
`callback(err, transformedString)` function as the last parameter.

Check the source code for examples on extending the API.
