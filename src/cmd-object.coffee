#
# Cmd-Object main library exports
#

# Require
async = require 'async'

# CmdObject Class
class CmdObject
    constructor: (@base) ->
        return new CmdObject(@base) if not (this instanceof CmdObject)

        @modifiers = []                # Modifiers are chains that start with
                                    #    selectors
        @currentModifier = null        # The currently bound modifier

        # Get all API methods
        for name, rawMethod of CmdObject.api
            # Make sure it's part of the API
            continue if not CmdObject.api.hasOwnProperty(name)

            # Wrap
            wrapped = (name, fn) ->
                try
                    # Does this have an init?
                    fn.init.call(@) if fn.init

                    # Get the arguments
                    argArr = [].slice.call arguments, 2

                    # Fix argument lengths
                    remaining = (fn.length - 1) - argArr.length
                    argArr.push null for [1..remaining] if remaining > 0

                    # Add call
                    weakBind = fn.bindWeak.apply fn, argArr
                    @currentModifier.push weakBind.call.bind weakBind
                catch err
                    throw "in #{name}(): #{err}"

                # Return
                return @

            # Apply wrapper
            @[name] = wrapped.bind @, name, rawMethod

    generate: (options, callback) ->
        # Commit last modifier
        #    if there is one
        if @currentModifier?
            @modifiers.push @currentModifier
            @currentModifier = null

        # Map out the values
        async.map @modifiers.slice()
            , (mod, mapcb) ->
                # Create entry function + list
                functionWaterfall = [(cb) -> cb null, options].concat mod

                # Waterfall the values
                async.waterfall functionWaterfall, (err, result) ->
                    if err and not err.ignore
                        mapcb err
                    else
                        mapcb null, (if err? and err.ignore then '' else result.join ' ')
            , (err, mapped) =>
                    if err then callback err
                    else
                        async.reject mapped
                            , (elem, rejectcb) ->
                                rejectcb elem.length is 0
                            , (filtered) =>
                                callback null, ([@base].concat filtered).join ' '

# API object
CmdObject.api = {}

# Selector
CmdObject.api.$ = (property, callback) ->
    # Is there a property?
    #   We do this so we can use the default() or
    #   always() methods.
    unless property?
        callback null, []
        return

    # Do we have that property?
    unless @hasOwnProperty property
        callback null, []
        return

    # Get the property
    prop = @[property]

    # Switch type
    switch
        when typeof prop in ['string', 'number', 'boolean'] then callback null, [String(prop)]
        when Array.isArray prop then callback null, prop
        when typeof prop is 'object' then callback null, prop
        else callback "not a valid object value type: #{property}='#{prop}' <#{typeof prop}>"

# Selector <init>
CmdObject.api.$.init = ->
    @modifiers.push(@currentModifier) if @currentModifier?
    @currentModifier = []

# Joiner
CmdObject.api.join = (delim = '', callback) ->
    vals = @vals()
    callback null, (if vals.length > 0 then [vals.join delim] else [])

# Prefixer
CmdObject.api.prefix = (prefix = '--', callback) ->
    async.map @vals()
        , (elem, mapcb) ->
            mapcb null, (prefix + elem)
        , callback

# Suffixer
CmdObject.api.suffix = (suffix = '', callback) ->
    async.map @vals()
        , (elem, mapcb) ->
            mapcb null, (elem + suffix)
        , callback

# Wrapper
CmdObject.api.wrap = (prefix = '"', suffix = '"', callback) ->
    suffix = prefix if arguments.length is 2
    async.map @vals()
        , (elem, mapcb) ->
            mapcb null, (prefix + elem + suffix)
        , callback

# Pairs
CmdObject.api.pairs = (separator = '=', wrapper = '"', callback) ->
    # Not an object?
    tipo = typeof @
    if tipo isnt 'object' or (tipo is 'object' and Array.isArray @)
        callback "pairs() selection is not an object: #{@}"
        return

    # Translate
    translated = []
    translated.push [key, val] for key, val of @ when @hasOwnProperty(key)

    # Map
    async.map translated
        , (pair, mapcb) ->
            mapcb null, "#{pair[0]}" +
                (if pair[1] then "#{separator}#{wrapper}#{pair[1]}#{wrapper}" else "")
        , callback

# Default
CmdObject.api.default = (elems = [], callback) ->
    # Do we have elements?
    unless @vals().length > 0
        elems = [elems] if typeof elems isnt 'object'
        callback null, elems
    else
        callback null, @

# Always
CmdObject.api.always = (elems = [], callback) ->
    # Fix and callback
    elems = [elems] if typeof elems isnt 'object'
    callback null, elems

# Flag
#   If one or more is true, then add the passed flag;
#   if not, then empty the ENTIRE array
CmdObject.api.flag = (flag = "1", callback) ->
    # Some?
    async.some @vals()
        , (elem, somecb) ->
            if typeof elem is 'string'
                somecb (elem in ['yes', 'true', 'on'])
            else
                somecb Boolean elem
        , (truth) ->
            callback null, (if truth then [flag] else [])

# Ease of operations
Object.prototype.vals = ->
    # Array?
    return @ if Array.isArray(@)

    # Return
    vals = []
    vals.push val for key, val of @ when @hasOwnProperty key
    return vals

# Export
module.exports = (base) -> new CmdObject base

# Weak bind
Function.prototype.bindWeak = ->
    # Apply
    args = [].slice.call arguments
    fn = @

    # Return
    return ->
        # Get arguments
        appargs = [].slice.call arguments
        newargs = args.concat appargs

        # Return
        return fn.apply @, newargs
